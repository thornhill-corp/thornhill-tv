import fs from 'node:fs';
import { build } from 'vite';
import { createHtmlPlugin } from 'vite-plugin-html';
import createVuePlugin from '@vitejs/plugin-vue';
import electronBuilder from 'electron-builder';
import config from './config.js';

(async () => {
    try { fs.rmSync('./dist', { recursive: true }); } catch {}
    try { fs.rmSync('./build', { recursive: true }); } catch {}
    await build({
        root: './src-vue',
        base: './',
        build: {
            outDir: '../dist'
        },
        plugins: [
            createHtmlPlugin({
                inject: {
                    data: {
                        title: config.title || (JSON.parse(fs.readFileSync('./package.json', 'utf8'))).name
                    }
                }
            }),
            createVuePlugin()
        ]
    });
    await electronBuilder.build({
        targets: {
            'linux': electronBuilder.Platform.LINUX.createTarget(),
            'win32': electronBuilder.Platform.WINDOWS.createTarget(),
            'darwin': electronBuilder.Platform.MAC.createTarget()
        }[process.platform],
        config: {
            files: fs
                .readdirSync('.', { withFileTypes: true })
                .filter(item => ![
                    '.git',
                    '.gitignore',
                    'build.mjs',
                    'config.example.js',
                    'yarn.lock',
                    ...fs
                        .readFileSync('./.gitignore', 'utf8')
                        .split('\n')
                        .filter(item => !['dist', 'node_modules', 'config.js'].includes(item))
                ].includes(item.name))
                .map(item => `${item.name}${item.isDirectory() ? '/**' : ''}`),
            directories: {
                output: 'build',
                buildResources: 'src-electron/assets'
            },
            win: {
                target: [{
                    target: 'nsis',
                    arch: 'x64'
                }],
            },
            linux: {
                target: [{
                    target: 'AppImage',
                    arch: 'x64'
                }],
            },
            mac: {
                target: [{
                    target: 'dmg'
                }]
            }
        }
    });
})().catch(console.error);