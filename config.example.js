module.exports = {
    /** @type {boolean} */
    isSpellcheckEnabled: false,
    /** @type {boolean} */
    isTitleBarEnabled: false,
    /** @type {boolean} */
    isMenuBarEnabled: false,
    /** @type {boolean} */
    isCorsEnabled: false,
    /** @type {string} */
    title: 'Thornhill TV'
};