const
    electron = require('electron'),
    {
        isSpellcheckEnabled,
        isTitleBarEnabled,
        isMenuBarEnabled,
        isCorsEnabled,
        title = require('./package.json').name
    } = require('./config.js');

(async () => {
    if(typeof electron === 'string'){
        const
            net = require('node:net'),
            { spawn } = require('node:child_process'),
            { createServer } = require('vite'),
            createVuePlugin = require('@vitejs/plugin-vue'),
            { createHtmlPlugin } = require('vite-plugin-html'),
            port = await new Promise(resolve => {
                const server = net.createServer();
                server.listen(
                    0,
                    () => {
                        const { port } = server.address();
                        server.close(() => resolve(port));
                    }
                );
            });
        await (await createServer({
            root: './src-vue',
            server: {
                port,
                strictPort: true
            },
            plugins: [
                createHtmlPlugin({
                    inject: {
                        data: {
                            title
                        }
                    }
                }),
                createVuePlugin()
            ]
        })).listen();
        const child = spawn(
            electron,
            ['start.js'],
            {
                stdio: 'inherit',
                env: {
                    ...process.env,
                    'PORT': port
                }
            }
        );
        child.on('close', () => process.exit());
    }
    else {
        const
            path = require('node:path'),
            {
                default: installExtension
            } = electron.app.isPackaged ? {} : require('electron-devtools-installer');
        electron.nativeTheme.themeSource = 'dark';
        await electron.app.whenReady();
        if(!electron.app.isPackaged)
            await installExtension('nhdogjmejiglipccpnnnanhbledajbpd');
        const mainWindow = new electron.BrowserWindow({
            webPreferences: {
                preload: path.join(__dirname, './src-electron/preload.js'),
                spellcheck: isSpellcheckEnabled
            },
            icon: path.join(__dirname, './src-electron/assets/icon.png'),
            frame: isTitleBarEnabled,
            show: false
        });
        mainWindow.setMenuBarVisibility(isMenuBarEnabled);
        if(!isCorsEnabled){
            const corsHeaders = [
                'access-control-allow-origin',
                'access-control-allow-methods',
                'access-control-allow-headers'
            ];
            mainWindow.webContents.session.webRequest.onHeadersReceived((details, callback) => callback({
                responseHeaders: {
                    ...Object.fromEntries(
                        Object
                            .entries(details.responseHeaders)
                            .filter(([header]) => !corsHeaders.includes(header.toLowerCase()))
                    ),
                    ...Object.fromEntries(corsHeaders.map(header => [header, ['*']]))
                }
            }));
        }
        mainWindow.webContents.openDevTools({
            mode: 'detach',
            activate: !electron.app.isPackaged
        });
        if(electron.app.isPackaged){
            mainWindow.webContents.closeDevTools();
            await mainWindow.loadFile('./dist/index.html');
        }
        else
            await mainWindow.loadURL(`http://localhost:${process.env['PORT']}/`);
        mainWindow.show();
    }
})().catch(console.error);